#ifndef GATEWAY_H
#define GATEWAY_H



#define MAX_NODES 16 			/* Define o máximo de sensores que o gateway irá armazenar */
#define MAX_COMMANDS 15;

enum state{INATIVE=0, GET_METRIC, SET_CONFIG, TEST} mode;
enum state_config{COLLECT_ALL, REAL_TIME_ALL, STOP_MEASURE} state_config;

struct store_node { 	
	struct store_node *next;		
	linkaddr_t addr; 				
};

struct gateway_config {
	int window_t;
	int probe_t;
	enum state_config state;
	int token;
};
/*
Struct que é processada pelo gateway. 
GET = 1 {
	0 -> COLLECT_ALL
	1 -> REAL_TIME_ALL
	2 -> STOP_MEASURE	
}

SET = 2 {
	DEFINIR TEMPO DE JANELA E PROBE (Implementar depois).
}

TEST  =  3

*/
  

struct command{
	char item;
	struct command * next;
};

char comando [8];
void link_quality_estimator();
void gateway_send_request(void);
void gateway_send_config();

static struct command commands;
static struct gateway_config gateway_config;
static struct broadcast_conn broadcast;
static struct unicast_conn uc;
static process_event_t EVENT_NEW_MSG;

char data_receive[16];
int pos = 0;
MEMB(memb_command, struct command, MAX_NODES);
LIST(list_command);


MEMB(memb_node, struct store_node, MAX_NODES);
LIST(node_list);

#endif
