#ifndef GATEWAY_H
#define GATEWAY_H

struct mote_settings {
	int window_t;
	int probe_t;
	int timeout_t;
};

struct store_neighbors { 			/* This structure holds information about neighbors. */
	struct store_neighbor *next;
	linkaddr_t addr; 				// Adress	
	uint16_t last_rssi, last_lqi; 	// Last measures
	int num_pkt;
	double last_prr;
};

enum state{INATIVE=0, PROBE, COLLECT, REAL_TIME, STOP_MEASURE, TEST} state_controller;
char msg[256];
#define MAX_NEIGHBORS 16 			/* This #define defines the maximum amount of neighbors we can remember. */
MEMB(neighbors_memb, struct store_neighbors, MAX_NEIGHBORS);
LIST(neighbors_list);
static struct mote_settings mote_settings;
static process_event_t EVENT_NEW_METRIC;



#endif