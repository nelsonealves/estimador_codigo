#include "contiki.h"
#include "lib/random.h"
#include "net/rime/rime.h"
#include "lib/list.h"
#include "lib/memb.h"
#include "dev/leds.h"
#include "motes.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "sys/timer.h"




PROCESS(controller, "Starting general controller.");
PROCESS(probe, "Starting probing.");
PROCESS(window, "Starting windows.");
AUTOSTART_PROCESSES(&controller);

static struct broadcast_conn broadcast;
static struct unicast_conn uc;

/*--------------->  CALLBACKS  <---------------*/
static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from){
	struct store_neighbors *n;
	struct broadcast_message *m;
	for(n = list_head(neighbors_list); n != NULL; n = list_item_next(n)) {
		if(linkaddr_cmp(&n->addr, from)) break;
	}
	
	if(n == NULL) { 
		n = memb_alloc(&neighbors_memb);
		if(n == NULL) return;
		linkaddr_copy(&n->addr, from);
		n->num_pkt = 0;
		printf("Cara novo na area\n");
		list_add(neighbors_list, n);
	}

	n->last_rssi = packetbuf_attr(PACKETBUF_ATTR_RSSI);
	n->last_lqi = packetbuf_attr(PACKETBUF_ATTR_LINK_QUALITY);
	n->num_pkt++;

	leds_toggle(LEDS_YELLOW);
	
	printf("broadcast message received from %d.%d with RSSI %u, LQI %u, num_pkt: %d, msg: %s\n", from->u8[0], from->u8[1], 
	packetbuf_attr(PACKETBUF_ATTR_RSSI),packetbuf_attr(PACKETBUF_ATTR_LINK_QUALITY), n->num_pkt, (char *)packetbuf_dataptr());
}

static void broad_send(struct broadcast_conn *ptr, int status, int num_tx){
	printf("Enviando broadcast\n");
}

void send_probe() {
	packetbuf_copyfrom("Probe", 5);
    broadcast_send(&broadcast);
    leds_toggle(LEDS_YELLOW);
}

void calculate_prr() {
	struct store_neighbors *n;
	for(n = list_head(neighbors_list); n != NULL; n = list_item_next(n)) {
		printf("num_pkt: %d\n", n->num_pkt);
		n->last_prr = 0.1*(float)n->num_pkt;
		printf("De: %d.%d, prr= %f \n", n->addr.u8[0], n->addr.u8[1], n->last_prr);
		n->num_pkt =0;
	}	
}

static void recv_uc(struct unicast_conn *c, const linkaddr_t *from) {
  printf("unicast message received from %d.%d\n",
	 from->u8[0], from->u8[1]);
}

static void sent_uc(struct unicast_conn *c, int status, int num_tx) {
	const linkaddr_t *dest = packetbuf_addr(PACKETBUF_ADDR_RECEIVER);
	if(linkaddr_cmp(dest, &linkaddr_null)) 	return;
	printf("unicast message sent to %d.%d: status %d num_tx %d\n",dest->u8[0], dest->u8[1], status, num_tx);
}

/* Define callback when receive a broadcast */
static const struct broadcast_callbacks broadcast_call = {broadcast_recv, broad_send};
static const struct unicast_callbacks unicast_callbacks = {recv_uc, sent_uc};

PROCESS_THREAD(controller, ev, data){

	PROCESS_BEGIN();
		static struct etimer et;
		node_id_burn(1);
		EVENT_NEW_METRIC = process_alloc_event();
		
		unicast_open(&uc, 146, &unicast_callbacks);
		broadcast_open(&broadcast, 129, &broadcast_call);
		
		process_start(&window, NULL);
		process_start(&probe, NULL);
		mote_settings.window_t = 10;
		mote_settings.probe_t = 1;

		while(1){
			etimer_set(&et, 5 * CLOCK_SECOND);
			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
			printf("controller\n");
		}
	PROCESS_END();
}

PROCESS_THREAD(window, ev, data){

	PROCESS_BEGIN();
		static struct etimer window_t;
		while(1){
			etimer_set(&window_t, mote_settings.window_t * CLOCK_SECOND);
			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&window_t) || ev == EVENT_NEW_METRIC);
			if(ev == EVENT_NEW_METRIC){
				printf("Evento de novo valor de janela\n");
				break;
			} 
			printf("window\n");
			calculate_prr();
		}

	PROCESS_END();
}

PROCESS_THREAD(probe, ev, data){

	PROCESS_BEGIN();
		static struct etimer probe_t;
		while(1){
			etimer_set(&probe_t, mote_settings.probe_t * CLOCK_SECOND);
			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&probe_t) || ev == EVENT_NEW_METRIC);
			if(ev == EVENT_NEW_METRIC){
				printf("Evento de novo valor de janela\n");
				break;
			} 
			printf("probe\n");
			send_probe();
		}

		
	PROCESS_END();
}