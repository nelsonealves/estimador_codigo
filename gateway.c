#include "contiki.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "lib/random.h"
#include "net/rime/rime.h"
#include "dev/leds.h"
#include "net/netstack.h"
#include "dev/rs232.h"
#include "node-id.h"
#include "lib/list.h"
#include "lib/memb.h"

#include "gateway.h"

PROCESS(gateway, "Gateway init");
AUTOSTART_PROCESSES(&gateway);


static int rx_input_byte(unsigned char c){ /*Função que processa mensagem que chega pela serial*/
	data_receive[pos++] = c;	
	leds_toggle(LEDS_RED);
	if(c=='\0' || c=='\n'){
		data_receive[--pos]='\0';
		list_init(list_command);
		printf("palavra:");
		int test;
		for(test = 0; test < pos; test ++){
			printf("%c", data_receive[test]);
		}
		printf("\n");
		link_quality_estimator();
		return 1;
	}
	printf("tamanho do pos: %d\n", pos);
 	return 1; 
}
void send_broadcast(char * msg){
	printf("Enviando mensagem: %s\n", msg);
	packetbuf_copyfrom(msg, strlen(msg)+1);
	broadcast_send(&broadcast);
}
void process_command(void){
	struct command * n;
	struct command * x;
	printf("process_command\n");
	char * token;
	token = strtok(data_receive, "/");

	while(token != NULL){
		//strcat(token, "\0");
		printf("token: %s\n", token);

		n = memb_alloc(&memb_command);
		if(n == NULL) return;
		
		list_add(list_command, n);
		n->item = * token;
		printf("n->item: %s\n", n->item);
		token = strtok(NULL, "/");
		
	}

	for(x = list_head(list_command); x != NULL; x = list_item_next(x)) {
		printf("comando: %c\n", x->item);
	}	

}

void link_quality_estimator(){
	process_command();
	printf("LQE");
	// int token = rand() % 100;
	// char tok[20]; 
	// itoa(token, tok, 10);
	// printf("Attribute: %s\n", commands.attribute1);
	// printf("Value: %s\n", commands.value1);
	// usar while
	switch(mode){
		case INATIVE:
			
			if(mode != INATIVE) link_quality_estimator();
			break;
		case GET_METRIC:
			printf("GET_METRIC\n");
			gateway_send_request();
			mode = INATIVE;
			break;
		case SET_CONFIG:
			printf("SET_CONFIG\n");
			gateway_send_config();
			mode = INATIVE;
			break;
		case TEST:
			printf("Hi, i'm here!");
			mode = INATIVE;
			break;
	}

	memset(data_receive, 0, sizeof (data_receive));
	pos = 0;
}

void gateway_send_request(void){
	enum state_config state_config;
	
	// if(strcmp("0", commands.value1)==0)	state_config = COLLECT_ALL;
	// if(strcmp("1", commands.value1)==0)	state_config = REAL_TIME_ALL;
	// if(strcmp("2", commands.value1)==0)	state_config = STOP_MEASURE;
	
	switch(state_config){
		case COLLECT_ALL:
			printf("GET=COLLECT_ALL\n");
			//send_broadcast("GET=COLLECT_ALL");
			packetbuf_copyfrom("GET=COLLECT_ALL", strlen("GET=COLLECT_ALL")+1);
			broadcast_send(&broadcast);
			int i = broadcast_send(&broadcast);
			printf("valor do retorno: %d\n",i);
			break;
		case REAL_TIME_ALL:
			printf("GET=REAL_TIME_ALL\n");
			send_broadcast("GET=REAL_TIME_ALL");
			break;
		case STOP_MEASURE:
			printf("GET=STOP_MEASURE\n");
			send_broadcast("GET=STOP_MEASURE");
			break;
	}
	//free(msg);
}

void sendBroadcast(char * msg){
	packetbuf_copyfrom(msg, strlen(msg)+1);
	int i = broadcast_send(&broadcast);
	printf("valor do retorno: %d\n",i);
}

void gateway_send_config(){
	char * msg = malloc(sizeof(data_receive)+32);
	printf("gateway_send_config\n");
	
	// printf("Antes: \n");
	// printf("msg: %s\n", msg);
	// strcat(msg, commands.attribute1);
	// strcat(msg, "=");
	// printf("msg: %s\n", msg);
	// strcat(msg, commands.value1);
	// strcat(msg, ";");
	// printf("msg: %s\n", msg);
	// strcat(msg, commands.attribute2);
	// printf("msg: %s\n", msg);
	// strcat(msg, "=");
	// strcat(msg, commands.value2);
	// strcat(msg, ";");
	// strcat(msg, "token=");
	// strcat(msg, token);
	// strcat(msg, ";");
	// printf("msg: %s\n", msg);

	free(msg);
}

/*--------------- CALLBACKS ------------------*/

static void broadcast_recv(struct broadcast_conn *c, const linkaddr_t *from){

	//printf("Gateway recebeu broadcast\n");
	leds_toggle(LEDS_YELLOW);
}
static void broad_send (struct broadcast_conn *ptr, int status, int num_tx){
	printf("Enviado broadcast\n");
}

static void recv_uc(struct unicast_conn *c, const linkaddr_t *from) {
	struct store_node *n;
	
	for(n = list_head(node_list); n != NULL; n = list_item_next(n)) {
		if(linkaddr_cmp(&n->addr, from)) break;
	}
	
	if(n == NULL) { 
		n = memb_alloc(&memb_node);
		if(n == NULL) return;
		linkaddr_copy(&n->addr, from);
		printf("Cara novo na area\n");
		list_add(node_list, n);
	}

	leds_toggle(LEDS_YELLOW);
	
	printf("broadcast message received from %d.%d ", from->u8[0], from->u8[1]);
}
/*---------------------------------------------------------------------------*/
static void sent_uc(struct unicast_conn *c, int status, int num_tx) {
//   const linkaddr_t *dest = packetbuf_addr(PACKETBUF_ADDR_RECEIVER);
//   if(linkaddr_cmp(dest, &linkaddr_null)) {
//     return;
//   }
//   printf("unicast message sent to %d.%d: status %d num_tx %d\n",
//     dest->u8[0], dest->u8[1], status, num_tx);
}

static const struct unicast_callbacks unicast_callbacks = {recv_uc, sent_uc};
static const struct broadcast_callbacks broadcast_call = {broadcast_recv, broad_send};

PROCESS_THREAD(gateway, ev, data){
	static struct etimer et;
	PROCESS_EXITHANDLER(broadcast_close(&broadcast));

PROCESS_BEGIN();
	node_id_burn(100);
	broadcast_open(&broadcast, 129, &broadcast_call);
	unicast_open(&uc, 146, &unicast_callbacks);
	rs232_set_input(RS232_PORT_0, rx_input_byte);
	leds_on(LEDS_GREEN);
	
	while(1){
			etimer_set(&et, 2 * CLOCK_SECOND);
			PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
			//printf("controller\n");
		}
PROCESS_END();
}

